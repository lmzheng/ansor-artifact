# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
# pylint: disable=unused-variable,invalid-name

"""
Integrate ansor into relay. It implements the following items:
1. Extract search tasks from a relay program
2. Provide auto-scheduling for all TOPI compute functions
"""
import os
import json
import threading

import tvm
from tvm import te, transform
from tvm.te.tensor import PlaceholderOp, ComputeOp
from . import _ffi_api
from .dispatcher import DispatchContext, FallbackConfig
from .workload_registry import register_workload_bufs, compute_dag_hash
from .compute_dag import ComputeDAG, LayoutRewriteLevel
from .env import GLOBAL_SCOPE

def call_all_topi_funcs(mod, target, params, target_host=None):
    """Call all TOPI compute + schedule to extract tasks in a relay program"""
    # pylint: disable=import-outside-toplevel
    from tvm import relay
    from tvm.relay.backend import graph_runtime_codegen

    with transform.PassContext(opt_level=3, disabled_pass={"KernelLayoutTransform"}):
        opt_mod, _ = relay.optimize(mod, target, params)
        grc = graph_runtime_codegen.GraphRuntimeCodegen(None, target)
        grc.codegen(opt_mod["main"])

def extract_from_program(mod, params, target, target_host=None):
    """ Extract tuning tasks from a relay program.

    This function is the single program version of extract_from_multiple_program.

    Parameters
    ----------
    mod : relay.Module
        The module to extract.
    params: dict of str to numpy array
        The associated parameters of the program
    ops: List of relay op
        List of relay ops to be tuned
    target: tvm.target.Target
        The compilation target
    target_host: tvm.target.Target
        The host compilation target

    Returns
    -------
    workloads: Array of Tuple(wkl_key, target)
    """
    return extract_from_multiple_program([mod], [params], target, target_host)

def extract_from_multiple_program(mods, params, target, target_host=None):
    """ Extract tuning tasks from multiple relay programs.

    Parameters
    ----------
    mods : List of relay.Module
        The modules to extract.
    params: List of dict of str to numpy array
        The associated parameters of the programs
    ops: List of relay op
        List of relay ops to be tuned
    target: tvm.target.Target
        The compilation target
    target_host: tvm.target.Target
        The host compilation target

    Returns
    -------
    workloads: Array of Tuple(wkl_key, target)
    """
    # pylint: disable=import-outside-toplevel
    from tvm import relay

    env = TracingEnvironment(TracingMode.EXTRACT_TASK)
    with env:
        # run compiler to collect all TOPI calls during compilation
        for mod, param in zip(mods, params):
            # wrap build call in a new thread to avoid the conflict
            # between python's multiprocessing and tvm's thread pool
            build_thread = threading.Thread(target=call_all_topi_funcs,
                                            args=(mod, target, param, target_host))
        build_thread.start()
        build_thread.join()
        relay.backend.compile_engine.get().clear()

    # create tasks for target
    wkl_keys = []
    wkl_weights = []
    for wkl_key, wkl_weight in env.wkl_key_collection.items():
        wkl_keys.append(wkl_key)
        wkl_weights.append(wkl_weight)

    return wkl_keys, wkl_weights


class TracingMode:
    """Two modes for tracing"""
    EXTRACT_TASK = 0            # trace all topi calls to extract tasks
    PREPARE_LAYOUT_REWRITE = 1  # trace all topi calls to prepare layout rewrite


class TracingEnvironment:
    """Global environment for tracing all topi function calls"""
    current = None

    def __init__(self, tracing_mode):
        self.tracing_mode = tracing_mode
        self.relay_disable_build_cache = "false"
        self.wkl_key_collection = {}

    def __enter__(self):
        self.relay_disable_build_cache = os.environ.get("TVM_RELAY_DISABLE_BUILD_CACHE", "false")
        os.environ["TVM_RELAY_DISABLE_BUILD_CACHE"] = "true"
        TracingEnvironment.current = self
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.environ["TVM_RELAY_DISABLE_BUILD_CACHE"] = self.relay_disable_build_cache
        TracingEnvironment.current = None

    def add_workload_key(self, key):
        """Add the workload key of an Ansor search task

        Parameters
        ----------
        key: str
        """
        if key in self.wkl_key_collection:
            self.wkl_key_collection[key] += 1
        else:
            self.wkl_key_collection[key] = 1


@tvm._ffi.register_func("ansor.enter_layout_rewrite")
def _enter_layout_rewrite(*args):
    env = TracingEnvironment(TracingMode.PREPARE_LAYOUT_REWRITE)
    env.__enter__()


@tvm._ffi.register_func("ansor.exit_layout_rewrite")
def _exit_layout_rewrite(*args):
    env = TracingEnvironment.current
    env.__exit__(None, None, None)


def traverse_to_get_io_tensors(outs):
    """Traverse from a list of output tensors to get a whole computational DAG"""
    layout_free_ops = []
    inputs = []

    visited = set()

    def traverse(t):
        if t in visited:
            return
        if isinstance(t.op, PlaceholderOp):
            inputs.append(t)
        elif isinstance(t.op, ComputeOp):
            if "layout_free_placeholders" in t.op.attrs:
                layout_free_ops.append(t.op)
            for x in t.op.input_tensors:
                traverse(x)
        visited.add(t)

    for t in outs:
        traverse(t)

    has_layout_free = (len(layout_free_ops) > 0)
    return inputs + [t for t in outs], has_layout_free


def auto_schedule_topi(outs):
    """ Use ansor to auto-schedule a topi compute declaration """
    io_tensors, has_layout_free = traverse_to_get_io_tensors(outs)
    key = register_workload_bufs(io_tensors)

    # only enable layout rewrite for cpu backend
    enable_layout_rewrite = "cpu" in tvm.target.Target.current().keys

    env = TracingEnvironment.current
    if env is None:  # in the final build mode
        state = DispatchContext.current.query(tvm.target.Target.current(), key)
        if isinstance(state, FallbackConfig):
            return te.create_schedule([x.op for x in outs])

        dag = ComputeDAG(io_tensors)
        schedule, _ = dag.apply_steps_from_state(state)
        return schedule
    elif env.tracing_mode == TracingMode.EXTRACT_TASK:  # in the task extraction mode
        env.add_workload_key(key)
        return te.create_schedule([x.op for x in outs])
    elif env.tracing_mode == TracingMode.PREPARE_LAYOUT_REWRITE:
        # in prepare_layout_rewrite mode
        if enable_layout_rewrite and has_layout_free:
            dispatch_ctx = DispatchContext.current
            target = tvm.target.Target.current()
            state = dispatch_ctx.query(target, key)
            if isinstance(state, FallbackConfig):
                return te.create_schedule([x.op for x in outs])

            # rewrite the layout and update the context
            # for the new dag
            dag = ComputeDAG(outs)
            new_dag = dag.rewrite_layout_from_state(state)
            new_key = json.dumps((compute_dag_hash(new_dag),))
            if new_key != key:
                dispatch_ctx.update(target, new_key, state)
        return te.create_schedule([x.op for x in outs])
    else:
        raise ValueError("Invalid tracing mode: " + env.tracing_mode)

